import Noticia1 from './Noticia1.jpg';
import Noticia2 from './Noticia2.jpg';
import Noticia3 from './Noticia3.jpg';
import Noticia4 from './Noticia4.jpg';
import Noticia5 from './Noticia5.jpg';
import Noticia6 from './Noticia6.jpg';
import Noticia7 from './Noticia7.jpg';
import Noticia8 from './Noticia8.jpg';

export default {
    "img1": Noticia1,
    "img2": Noticia2,
    "img3": Noticia3,
    "img4": Noticia4,
    "img5": Noticia5,
    "img6": Noticia6,
    "img7": Noticia7,
    "img8": Noticia8
}